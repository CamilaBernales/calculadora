/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesDTO;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author PERSONAL
 */
@Entity 
@Table (name= "AbonoPropietario")
public class AbonoPropietario {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idAbonoPropietario; 
   @Column
    private Date fechaHora;
   @Column
    private BigDecimal montoCobrado;
   @Column
    private int nroComprobante;
   @Column
    private BigDecimal saldoActual;
   @Column
    private Propietario propietario;
    ;

    public AbonoPropietario(Date fechaHora, BigDecimal montoCobrado, int nroComprobante, BigDecimal saldoActual, Propietario propietario) {
        this.fechaHora = fechaHora;
        this.montoCobrado = montoCobrado;
        this.nroComprobante = nroComprobante;
        this.saldoActual = saldoActual;
        this.propietario = propietario;
       
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public BigDecimal getMontoCobrado() {
        return montoCobrado;
    }

    public void setMontoCobrado(BigDecimal montoCobrado) {
        this.montoCobrado = montoCobrado;
    }

    public int getNroComprobante() {
        return nroComprobante;
    }

    public void setNroComprobante(int nroComprobante) {
        this.nroComprobante = nroComprobante;
    }

    public BigDecimal getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(BigDecimal saldoActual) {
        this.saldoActual = saldoActual;
    }

    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }
}
