/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import ClasesDAO.PropietarioDAO;
import ClasesDTO.Propietario;
import java.util.List;

/**
 *
 * @author PERSONAL
 */
public class TestPropietario {
    
    PropietarioDAO propietarioDao = new PropietarioDAO();
    private Propietario propietarioRestaurado = null;
    private int idAEliminar= 0;
    
    public void UsarDaoPropietario (){
        //instanciamos tres objetos de tipo propietario
        
        Propietario prop1 = new Propietario("Anahi", "Guzman", 34567890);
        Propietario prop2 = new Propietario("Araceli", "Gonzales", 23657832);
        Propietario prop3 = new Propietario("Maria", "Arevalo", 43597760);
        
        idAEliminar = propietarioDao.guardarPropietario(prop3);
        propietarioDao.guardarPropietario(prop1);
        propietarioDao.guardarPropietario(prop2);
        //modificamos al propietario 2 y lo actualizamos
        prop2.setNombre("Martin");
        propietarioDao.ActualizarPropietario(prop2);
       
        //recuperamos al propietario 1 de la base de datos
        propietarioRestaurado = propietarioDao.ObtenPropietario(idAEliminar);
        System.out.println("recuperamos al propietario:  " + propietarioRestaurado.getNombre());
        
        // eliminamos al propietario recuperado que es el 3
        
        propietarioDao.eliminarPropietario(prop3);
        
        //Obtenemos la lista de propietarios que quedan en la base de datos y la mosttamos
        
        List<Propietario> listaPropietarios = propietarioDao.obtenListaPropietario();
        System.out.println("Hay  " + listaPropietarios.size() + "propietarios en la base de datos");
        
        for(Propietario c : listaPropietarios){
            System.out.println(c.getNombre());
        }
        
        
        
        
        
        
        
        
    }
    
    
    
    
}
