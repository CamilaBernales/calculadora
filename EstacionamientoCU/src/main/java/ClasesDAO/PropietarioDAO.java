/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClasesDAO;


import ClasesDTO.Propietario;
import Hibernate.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author PERSONAL
 */
public class PropietarioDAO  {
    
    
    private Session session; 
    private Transaction tx; 
  
  
    
    private void IniciarOperacion() throws HibernateException {

        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();

    }

    private void manejaException(HibernateException he) throws HibernateException {
        tx.rollback();
        
       throw new HibernateException("Ocurrio un error al intentar acceder a capa de datos", he);
    }

    
    
    public int guardarPropietario(Propietario propietario){ 
        int idPropietario= 0;  

        try { 
            IniciarOperacion(); 
            idPropietario = (int)session.save(propietario); 
            tx.commit(); 
        }catch(HibernateException he) { 
            manejaException(he);
            throw he; 
        }finally { 
            session.close();
                    }
        return idPropietario; 
    } 

    
    public void ActualizarPropietario(Propietario propietario) throws HibernateException {
        try {
            IniciarOperacion();
            session.update(propietario);
            tx.commit();
        } catch (HibernateException he) {
            manejaException(he);
            throw he;
        } finally {
            session.close();
        }

    }

    

    public void eliminarPropietario(Propietario propietario) {
        try {
            IniciarOperacion();
            session.delete(propietario);
            tx.commit();
        } catch (HibernateException he) {
            manejaException(he);
            throw he;
        } finally {
            session.close();
        }
    }

    public Propietario ObtenPropietario(long idPropietario) {
        
 Propietario propietario = null;  
        try 
        { 
            IniciarOperacion(); 
            propietario = (Propietario) session.get(Propietario.class, idPropietario); 
        } finally 
        { 
            session.close(); 
        }  

        return propietario; 
    }  
public List<Propietario> obtenListaPropietario() throws HibernateException 
    { 
        List<Propietario> listaPropietario = null;  

        try 
        { 
            IniciarOperacion(); 
            listaPropietario = session.createQuery("from Propietario").list(); 
        } finally 
        { 
            session.close(); 
        }  

        return listaPropietario; 
    }  
    



}
