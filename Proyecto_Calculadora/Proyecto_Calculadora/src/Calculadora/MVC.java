/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculadora;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author PERSONAL
 */
public class MVC extends JPanel {
//vista

    public MVC() {
        principio = true;
        setLayout(new BorderLayout());

        visor = new JTextField("0");
        visor.setEditable(false);
        visor.setForeground(Color.blue);
        visor.setFont(new Font("1942 report", Font.BOLD, 14));

        add(visor, BorderLayout.NORTH);

        panel2 = new JPanel();
        panel2.setLayout(new GridLayout(4, 4));

        ActionListener modelo = new Modelo();
        /* JButton boton2= new JButton("2");
        miLamina2.add(boton2);
           JButton boton3= new JButton("3");
        miLamina2.add(boton3);
           JButton boton4= new JButton("4");
        miLamina2.add(boton4);* modo normal de agregar botones */
        ActionListener insertar = new Controlador();

        ponerBoton("7", insertar);
        ponerBoton("8", insertar);
        ponerBoton("9", insertar);
        ponerBoton("/", modelo);
        ponerBoton("4", insertar);
        ponerBoton("5", insertar);
        ponerBoton("6", insertar);
        ponerBoton("*", modelo);
        ponerBoton("1", insertar);
        ponerBoton("2", insertar);
        ponerBoton("3", insertar);
        ponerBoton("-", modelo);
        ponerBoton("0", insertar);
        ponerBoton(".", insertar);
        ponerBoton("+", modelo);
        ponerBoton("=", modelo);
        //ponerBoton("C", modelo); NO FUNCIONA ESTE BOTON

        add(panel2, BorderLayout.CENTER);

        ultimaOperacion = "=";

    }

    // modo alternativo para no generar tanto codigo
    private void ponerBoton(String rotulo, ActionListener oyente) {
        boton = new JButton(rotulo);
        boton.setForeground(Color.blue);
        boton.setFont(new Font("1942 report", Font.BOLD, 14));

        boton.addActionListener(oyente);
        panel2.add(boton);

        // FIN VISTA
    }
    private JPanel panel2;
    private JButton boton;
    private boolean principio;
    private double resultado;
    private String ultimaOperacion;
    private JTextField visor;

// CONTROLLER
    private class Controlador implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String entrada = e.getActionCommand();
            if (principio) {
                visor.setText("");
                principio = false;
            }
            visor.setText(visor.getText() + entrada);

        }

    }

    public class Modelo implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String operacion = e.getActionCommand();

            calcular(Double.parseDouble(visor.getText()));
            ultimaOperacion = operacion;

            principio = true;

        }

        public void calcular(double x) {

            if (ultimaOperacion.equals("+")) {
                resultado += x;

            } else if (ultimaOperacion.equals("-")) {
                resultado -= x;
            } else if (ultimaOperacion.equals("*")) {
                resultado *= x;

            } else if (ultimaOperacion.equals("/")) {

                resultado /= x;

            } else if (ultimaOperacion.equals("=")) {
                resultado = x;

                /* } else if(ultimaOperacion.equals("C")){
                String cadena;
                cadena= visor.getText();
                if(cadena.length()>0){
                    cadena = cadena.substring(0, cadena.length()-1);
                    visor.setText(cadena);
                }*/
            }
            visor.setText("" + resultado);

        }
    }
}
